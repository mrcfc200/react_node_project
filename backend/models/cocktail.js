const mongoose = require("mongoose");

const cocktail = new mongoose.Schema({
  id: { type: Number },
  name: { type: String },
  description: { type: String },
  ingredients: { type: Array },
  price: { type: Number },
  how: { type: Array },
  detailImage: { type: String },
});

const Cocktail = mongoose.model("cocktail", cocktail);

module.exports = Cocktail;
