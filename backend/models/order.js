const mongoose = require("mongoose");

const order = new mongoose.Schema(
  {
    table: { type: String },
    delivery: { type: Array },
  },
  { timestamps: true }
);

const Order = mongoose.model("order", order);

module.exports = Order;
