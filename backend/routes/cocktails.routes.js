const express = require("express");
const router = express.Router();

const Cocktail = require("../models/cocktail");

router.get("/", (req, res) => {
  return Cocktail.find().then((cocktails) => {
    return res.status(200).json(cocktails);
  });
});

router.get("/:id", async (req, res) => {
  const cocktail = req.params.id;
  const foundCocktail = await Cocktail.findOne({ id: cocktail });
  return res.status(200).json(foundCocktail);
});

module.exports = router;
