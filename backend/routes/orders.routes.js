const express = require("express");
const router = express.Router();

const Order = require("../models/order");

router.get("/", (req, res) => {
  return Order.find().then((orders) => {
    return res.status(200).json(orders);
  });
});

router.post("/newOrder", async (req, res) => {
  const { table, delivery } = req.body;
  const newOrder = new Order({
    table: table,
    delivery: delivery,
  });
  console.log("New order recieved => ", newOrder);
  const createdOrder = await newOrder.save();
  return res.status(201).json(createdOrder);
});

module.exports = router;
