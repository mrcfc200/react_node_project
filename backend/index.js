//? Main dependencies
const express = require("express");
const dbConnection = require("./database/dbConnection");

//? Routes dependencies
const cocktailsRouter = require("./routes/cocktails.routes");
const ordersRouter = require("./routes/orders.routes");
const loginRouter = require("./routes/auth.routes");

//? Others dependencies
const cors = require("cors");
const bodyParser = require("body-parser");

const port = 3001;
const server = express();

server.use(bodyParser.json());
server.use(bodyParser.urlencoded({ extended: true }));

dbConnection();

server.options("*", cors()); // include before other routes
server.use(cors());

server.use("/cocktailList", cocktailsRouter);
server.use("/orders", ordersRouter);
server.use("/auth", loginRouter);

server.listen(port, () => {
  console.log(`Server running in http://localhost:${port}`);
});
