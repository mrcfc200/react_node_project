import {
  fetchOrdersError,
  fetchOrdersRequest,
  fetchOrdersSuccess,
} from "../actions/orders.actions";

export const fetchOrders = () => {
  return (dispatch) => {
    dispatch(fetchOrdersRequest());
    try {
      fetch("http://127.0.0.1:3001/orders")
        .then((response) => response.json())
        .then((response) => dispatch(fetchOrdersSuccess(response)));
    } catch (error) {
      dispatch(fetchOrdersError(error));
    }
  };
};
