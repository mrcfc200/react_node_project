import {
  fecthCocktailsError,
  fetchCocktailsRequest,
  fetchCocktailsSucess,
} from "../actions/cocktails.actions";

export const fetchCocktails = () => {
  return (dispatch) => {
    dispatch(fetchCocktailsRequest());
    try {
      fetch("http://127.0.0.1:3001/cocktailList")
        .then((response) => response.json())
        .then((response) => dispatch(fetchCocktailsSucess(response)));
    } catch (error) {
      dispatch(fecthCocktailsError(error));
    }
  };
};
