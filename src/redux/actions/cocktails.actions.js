export const FETCH_COCKTAILS_REQUEST = "FETCH_COCKTAILS_REQUEST";
export const FETCH_COCKTAILS_SUCCESS = "FETCH_COCKTAILS_SUCCESS";
export const FECTH_COCKTAILS_ERROR = "FECTH_COCKTAILS_ERROR";

export const fetchCocktailsRequest = () => {
  return {
    type: FETCH_COCKTAILS_REQUEST,
  };
};

export const fetchCocktailsSucess = (cocktails) => {
  return {
    type: FETCH_COCKTAILS_SUCCESS,
    payload: cocktails,
  };
};

export const fecthCocktailsError = (error) => {
  return {
    type: FECTH_COCKTAILS_ERROR,
    payload: error,
  };
};
