import {
  FETCH_ORDERS_ERROR,
  FETCH_ORDERS_REQUEST,
  FETCH_ORDERS_SUCCESS,
} from "../actions/orders.actions";

const initialState = {
  loading: false,
  orders: [],
  error: "",
};

export const ordersReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_ORDERS_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case FETCH_ORDERS_SUCCESS:
      return {
        ...state,
        loading: false,
        orders: action.payload,
      };
    case FETCH_ORDERS_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    default:
      return action;
  }
};
