import { combineReducers } from "redux";
import { cocktailsReducer } from "./cocktails.reducer";
import { ordersReducer } from "./orders.reducer";

export const rootReducer = combineReducers({
  cocktails: cocktailsReducer,
  orders: ordersReducer,
});
