import {
  FETCH_COCKTAILS_REQUEST,
  FETCH_COCKTAILS_SUCCESS,
  FECTH_COCKTAILS_ERROR,
  UPDATE_STOCK,
} from "../actions/cocktails.actions";

const initialState = {
  loading: false,
  cocktails: [],
  error: "",
};

export const cocktailsReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_COCKTAILS_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case FETCH_COCKTAILS_SUCCESS:
      return {
        loading: false,
        cocktails: action.payload,
      };
    case FECTH_COCKTAILS_ERROR:
      return {
        loading: false,
        cocktails: [],
        error: action.payload,
      };

    default:
      return state;
  }
};
