import { Navbar, Nav, Container } from "react-bootstrap";
import { Link } from "react-router-dom";

import "./menu.scss";
const Menu = () => {
  return (
    <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
      <Container>
        <Navbar.Brand href="#home">Bar Management</Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse className="navbar-collapse">
          <Nav.Link as={Link} to="/">
            Home
          </Nav.Link>
          <Nav.Link as={Link} to="/orders">
            Orders
          </Nav.Link>
          <Nav.Link>Contability</Nav.Link>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};
export default Menu;
