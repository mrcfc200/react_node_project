import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import "./howTo.scss";
const HowTo = () => {
  const { id } = useParams();
  const [cocktailHow, setCocktailHow] = useState({});

  useEffect(() => {
    console.log(2);
    fetch(`http://127.0.0.1:3001/cocktailList/${id}`)
      .then((res) => res.json())
      .then((result) => {
        console.log(result);
        setCocktailHow(result);
      });
  }, [id]);

  return (
    <div className="howTo-div">
      <div className="cocktail-detail-div">
        <h1 className="cocktail-detail-title">{cocktailHow.name}</h1>
        <img
          className="cocktail-detail-image"
          src={cocktailHow.detailImage}
          alt={cocktailHow.name}
        />
        <p className="cocktail-detail-description">{cocktailHow.description}</p>
        <h2 className="cocktail-detail-subtitle">Vamos a prepararlo!</h2>
        <h3 className="cocktail-detail-subtitle-1">Ingredientes</h3>

        <ul className="cocktail-detail-ingredients-ul">
          {cocktailHow.ingredients?.map((ingredient) => (
            <li className="cocktail-detail-ingredients-li">{ingredient}</li>
          ))}
        </ul>
        <h3 className="cocktail-detail-subtitle-1">¿Y cómo lo hago?</h3>
        <p className="cocktail-detail-how">{cocktailHow.how}</p>
      </div>
    </div>
  );
};

export default HowTo;
