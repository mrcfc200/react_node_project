import { useNavigate } from "react-router-dom";
import { connect } from "react-redux";

import "./Home.scss";
import { store } from "../../redux/store/store";

const Home = ({ cocktails }) => {
  const displayParagraph = (id) => {
    const item = document.querySelector(`.paragraph-${id}`);

    if (item.style.display === "inline") {
      console.log("hola");
      item.style.display = "none";
    } else {
      item.style.display = "inline";
    }
  };

  const navigate = useNavigate();

  return (
    <div className="home-div">
      {cocktails.map((cocktail) => (
        <div className="cocktail-div" key={cocktail.id}>
          <div>
            <p
              onClick={() => {
                navigate(`/howTo/${cocktail.id}`);
              }}
            >
              ?
            </p>
            <p className={`cocktail-paragraph paragraph-${cocktail.id}`}>
              {cocktail.name}
            </p>
          </div>
          <img
            className={`cocktail-image img-${cocktail.id}`}
            src={cocktail.image}
            alt={cocktail.name}
            onClick={() => displayParagraph(cocktail.id)}
          />
        </div>
      ))}
    </div>
  );
};

const mapStateToProps = ({ cocktails }) => ({
  loading: cocktails.loading,
  cocktails: cocktails.cocktails,
});

export default connect(mapStateToProps)(Home);
