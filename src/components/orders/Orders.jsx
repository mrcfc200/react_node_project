import { useEffect, useState } from "react";
import { Form, Button } from "react-bootstrap";
import { connect } from "react-redux";

import "./orders.scss";

const Orders = ({ cocktails, orders }) => {
  const enableNumberInput = (id) => {
    const input = document.querySelector(`.number-input-${id}`);
    input.disabled ? (input.disabled = false) : (input.disabled = true);
  };

  const submitForm = (event) => {
    //event.preventDefault();

    const delivery = [];
    const table = document.querySelector(".option-selected").value;

    const checkBoxes = document.querySelectorAll(".checkbox-input");

    const numberCheckedBoxes = document.querySelectorAll(
      'input[type="checkbox"]:checked'
    ).length;

    if (numberCheckedBoxes) {
      checkBoxes.forEach((checkBox) => {
        if (checkBox.checked) {
          const numberInput = document.querySelector(
            `.number-input-${checkBox.id}`
          );
          delivery.push({ name: checkBox.value, quantity: numberInput.value });
        }
      });
      postForm(table, delivery);
    } else {
      console.log("No product selected");
    }
  };

  const postForm = async (table, delivery) => {
    console.log(delivery);
    fetch("http://127.0.0.1:3001/orders/newOrder", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ table, delivery }),
    });
  };

  return (
    <div className="orders-div">
      <div className="order-form-div">
        <Form className="order-form" onSubmit={submitForm}>
          <Form.Group className="order-form-group" controlId="OrderFormGroup">
            <Form.Label>Mesa</Form.Label>
            <select className="option-selected" name="table">
              {[...Array(10).keys()].map((number) => (
                <option key={number}>{number}</option>
              ))}
            </select>

            <Form.Label>Pedido</Form.Label>
            {cocktails.map(({ _id, id, name }) => (
              <div key={_id} className="delivery-div">
                <div className={`checkbox-div ${id}-checkbox-div`}>
                  <input
                    className="checkbox-input"
                    type="checkbox"
                    onClick={() => enableNumberInput(id)}
                    id={id}
                    name={name}
                    value={name}
                  />
                  <label className="checkbox-label">{name}</label>
                  <div className="input-number-div">
                    <input
                      className={`number-input number-input-${id}`}
                      type="number"
                      min="1"
                      max="100"
                      disabled
                      required
                    />
                  </div>
                </div>
              </div>
            ))}
            <input type="submit" value="Enviar" />
          </Form.Group>
        </Form>
      </div>

      {orders?.map(({ _id, table, delivery }) => (
        <div key={_id}>
          <p>Mesa {table}</p>
          {delivery.map((product) => (
            <p>
              {product.quantity} {product.name}
            </p>
          ))}
        </div>
      ))}
    </div>
  );
};

const mapStateToProps = ({ cocktails, orders }) => ({
  cocktails: cocktails.cocktails,
  orders: orders.orders,
});

export default connect(mapStateToProps)(Orders);
