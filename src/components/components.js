import Home from "./home/Home";
import Menu from "./menu/Menu";
import HowTo from "./howTo/HowTo";
import Orders from "./orders/Orders";
import Error from "./error/Error";

export { Home, Menu, HowTo, Orders, Error };
