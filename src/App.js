import { Error, Home, Menu, Orders, HowTo } from "./components/components";
import {
  Route,
  Routes,
  Navigate,
  BrowserRouter as Router,
} from "react-router-dom";

function App() {
  return (
    <Router>
      <Menu />
      <Routes>
        <Route path="/" element={<Navigate to="/home" />} />
        <Route path="/home" element={<Home />} />
        <Route path="/orders" element={<Orders />} />
        <Route path="/howTo/:id" element={<HowTo />} />
        <Route path="*" element={<Error />} />
      </Routes>
    </Router>
  );
}

export default App;
