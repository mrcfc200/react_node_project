import React from "react";
import ReactDOM from "react-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.bundle.min";
import App from "./App";
import { store } from "./redux/store/store";
import { fetchCocktails } from "./redux/functions/cocktails.function";
import { Provider } from "react-redux";
import { fetchOrders } from "./redux/functions/orders.functions";

store.dispatch(fetchCocktails());
store.dispatch(fetchOrders());

const ReduxApp = () => {
  return (
    <Provider store={store}>
      <App />
    </Provider>
  );
};

ReactDOM.render(<ReduxApp />, document.getElementById("root"));
